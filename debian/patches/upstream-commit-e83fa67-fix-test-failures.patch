From: John Johansen <john.johansen@canonical.com>
Date: Fri, 20 Apr 2018 17:27:47 -0700
Subject: parser: fix failures due to -M only setting compile-features

Split the features file into compile features and kernel features
which is needed for policy versioning and the new caching scheme.

A new flag --kernel-features was added to set the kernel features but
unfortunately -M, --features-file was setup to only specify the
compile features, when it used to effectively specify both the
compile and kernel features.

This broke existing uses of -M.

Fix this by having -M specify both the compile and kernel features,
and a new flag --compile-features that can be used to specify the
compile fature set separate from the kernel feature set.

sbeattie> fixed up error message to refer to compile features when
--compile-features argument fails.

Fixes: 9e48a5da5e10 ("parser: split kernel features from compile features.")
Signed-off-by: John Johansen <john.johansen@canonical.com>
Signed-off-by: Steve Beattie <steve.beattie@canonical.com>

PR: https://gitlab.com/apparmor/apparmor/merge_requests/104
---
 parser/parser_main.c | 21 ++++++++++++++++++++-
 1 file changed, 20 insertions(+), 1 deletion(-)

diff --git a/parser/parser_main.c b/parser/parser_main.c
index c7a752c..18fa774 100644
--- a/parser/parser_main.c
+++ b/parser/parser_main.c
@@ -129,6 +129,7 @@ struct option long_options[] = {
 	{"ofile",		1, 0, 'o'},
 	{"match-string",	1, 0, 'm'},
 	{"features-file",	1, 0, 'M'},
+	{"compile-features",	1, 0, 139},	/* no short option */
 	{"kernel-features",	1, 0, 138},	/* no short option */
 	{"quiet",		0, 0, 'q'},
 	{"skip-kernel-load",	0, 0, 'Q'},
@@ -185,7 +186,8 @@ static void display_usage(const char *command)
 	       "-I n, --Include n	Add n to the search path\n"
 	       "-f n, --subdomainfs n	Set location of apparmor filesystem\n"
 	       "-m n, --match-string n  Use only features n\n"
-	       "-M n, --features-file n Compile features set in file n\n"
+	       "-M n, --features-file n Set compile & kernel features to file n\n"
+	       "--compile-features n    Compile features set in file n\n"
 	       "--kernel-features n     Kernel features set in file n\n"
 	       "-n n, --namespace n	Set Namespace for the profile\n"
 	       "-X, --readimpliesX	Map profile read permissions to mr\n"
@@ -508,14 +510,21 @@ static int process_arg(int c, char *optarg)
 		}
 		break;
 	case 'M':
+		if (compile_features)
+			aa_features_unref(compile_features);
+		if (kernel_features)
+			aa_features_unref(kernel_features);
 		if (aa_features_new(&compile_features, AT_FDCWD, optarg)) {
 			fprintf(stderr,
 				"Failed to load features from '%s': %m\n",
 				optarg);
 			exit(1);
 		}
+		kernel_features = aa_features_ref(compile_features);
 		break;
 	case 138:
+		if (kernel_features)
+			aa_features_unref(kernel_features);
 		if (aa_features_new(&kernel_features, AT_FDCWD, optarg)) {
 			fprintf(stderr,
 				"Failed to load kernel features from '%s': %m\n",
@@ -523,6 +532,16 @@ static int process_arg(int c, char *optarg)
 			exit(1);
 		}
 		break;
+	case 139:
+		if (compile_features)
+			aa_features_unref(compile_features);
+		if (aa_features_new(&compile_features, AT_FDCWD, optarg)) {
+			fprintf(stderr,
+				"Failed to load compile features from '%s': %m\n",
+				optarg);
+			exit(1);
+		}
+		break;
 	case 'q':
 		conf_verbose = 0;
 		conf_quiet = 1;
